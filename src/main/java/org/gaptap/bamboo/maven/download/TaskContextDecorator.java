/*
 * Copyright 2013 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gaptap.bamboo.maven.download;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.HashMap;
import java.util.Map;

import com.atlassian.bamboo.build.logger.BuildLogger;
import com.atlassian.bamboo.configuration.ConfigurationMap;
import com.atlassian.bamboo.configuration.ConfigurationMapImpl;
import com.atlassian.bamboo.plugins.maven.task.AbstractMavenConfig;
import com.atlassian.bamboo.serialization.WhitelistedSerializable;
import com.atlassian.bamboo.task.CommonTaskContext;
import com.atlassian.bamboo.v2.build.CommonContext;
import com.davidehringer.bamboo.maven.extractor.InvalidPomException;
import org.jetbrains.annotations.Nullable;

/**
 * Decorates a CommonTaskContext and allows for replacing the "goals"
 * configuration with the dynamically determined goals needed for downloading
 * artifacts.
 * 
 * @author David Ehringer
 */
public class TaskContextDecorator implements CommonTaskContext {

    private final CommonTaskContext taskContext;
    private final ConfigurationMap configurationMap;

    public TaskContextDecorator(CommonTaskContext taskContext) throws FileNotFoundException, InvalidPomException {
        this.taskContext = taskContext;

        Map<String, String> config = new HashMap<String, String>();
        config.putAll(taskContext.getConfigurationMap());

        MvnDownloadGoal mvnCopy = MvnDownloadGoal.from(taskContext);
        config.put(AbstractMavenConfig.CFG_GOALS, mvnCopy.getCommandLine());
        configurationMap = new ConfigurationMapImpl(config);
    }

    @Override
    public long getId() {
        return taskContext.getId();
    }

    @Override
    public String getPluginKey() {
        return taskContext.getPluginKey();
    }

    @Override
    public String getUserDescription() {
        return taskContext.getUserDescription();
    }

    @Override
    public boolean isEnabled() {
        return taskContext.isEnabled();
    }

    @Override
    public boolean isFinalising() {
        return taskContext.isFinalising();
    }

    @Override
    public BuildLogger getBuildLogger() {
        return taskContext.getBuildLogger();
    }

    @Override
    public CommonContext getCommonContext() {
        return taskContext.getCommonContext();
    }

    @Override
    public ConfigurationMap getConfigurationMap() {
        return configurationMap;
    }

    @Override
    public File getRootDirectory() {
        return taskContext.getRootDirectory();
    }

    @Override
    public Map<String, String> getRuntimeTaskContext() {
        return taskContext.getRuntimeTaskContext();
    }

    @Nullable
    @Override
    public Map<String, WhitelistedSerializable> getRuntimeTaskData() {
        return taskContext.getRuntimeTaskData();
    }

    @Override
    public boolean doesTaskProduceTestResults() {
        return taskContext.doesTaskProduceTestResults();
    }

    @Override
    public File getWorkingDirectory() {
        return taskContext.getWorkingDirectory();
    }

}
